<?php
require "vendor/autoload.php";


$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views'); //a mettre en premier 
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);


Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");
    $twig->addFilter(new Twig_Filter('trad', function($string){
        return $string; // si on met "toto" a la place de $string alors il sera retourné (cf dans first_view contenu|trad )
    }));
});


Flight::route('/first_view/', function(){
    $data = [
        'contenu' => 'Hello World!',
        'name' => 'Ben Kenobi',
    ];
    Flight::view()->display('first_view.twig', $data);
});

Flight::route('/books', function (){
    $data = [
        "books" => recupererLivres(),
    ];
    Flight::view()->display('books.twig', $data);
});


Flight::route('/', function(){
    echo 'Hello World!';
});


Flight::route('/hello/@name', function($name){
    echo "<h2>Hello, $name!</h2>";

});




Flight::start();

